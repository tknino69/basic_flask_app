FROM python:3.6-alpine
RUN apk update && apk add curl
ADD . /src
WORKDIR /src
RUN pip install Flask requests requests-toolbelt
EXPOSE 5005:5005
CMD sed -i -e 's/\r//g' app.py && python app.py
